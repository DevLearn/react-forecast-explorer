/* @flow */

import axios from 'axios';
import {FetchWeatherAction} from "../types/types_actions";

const API_KEY = 'b7d30a4a5152cc657b1b851e64e734a7';
const BASE_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

export const FETCH_WEATHER = 'FETCH_WEATHER';

export const fetchWeather = (cityName): FetchWeatherAction => {
    const url = `${BASE_URL}&q=${cityName},br`;

    const request = axios.get(url);

    return {
        type: FETCH_WEATHER,
        payload: request
    }
};


