import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import ReduxPromise from 'redux-promise';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import './index.css';
import App from './App';
import reducers from './reducers';

import registerServiceWorker from './registerServiceWorker';

const middleware = applyMiddleware(ReduxPromise)(createStore);

ReactDOM.render(
    <Provider store={middleware(reducers)}>
    <App />
    </Provider>
    , document.getElementById('root'));
registerServiceWorker();
