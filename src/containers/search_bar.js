/* @flow */

import React, { Component } from "react";

import { fetchWeather } from '../actions/index';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

class SearchBar extends Component {
    state: {
        term: string
    };

    constructor(props) {
        super(props);

        this.state = {
            term: ''
        };
    }

    onChangeInput = (evt: Event) => {
        if (evt.target instanceof HTMLInputElement) {
            this.setState({term: evt.target.value});
        }
    };

    onFormSubmit = (evt: Event) => {
        evt.preventDefault();

        this.props.fetchWeather(this.state.term);
        this.setState({term: ''});
    };

    render() {
        return (
            <form className="form-inline" onSubmit={this.onFormSubmit}>
                <div className="row">
                <div className="form-group">
                    <input type="text" className="form-control" value={this.state.term} onChange={this.onChangeInput}/>
                </div>
                <button type="submit" className="btn btn-default">Send</button>
                </div>
            </form>
        );
    }
}

SearchBar.propTypes = {
    fetchWeather: Function
};
SearchBar.defaultProps = {};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({fetchWeather}, dispatch);
};

export default connect(null, mapDispatchToProps)(SearchBar);
