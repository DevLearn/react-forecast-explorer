import React, {Component} from 'react';
import {connect} from "react-redux";
import {WeatherData} from "../types/types_weather_api";

class WeatherList extends Component {
    props: {
        weather: [WeatherData]
    };
    
    renderCity(weatherData: WeatherData) {
        const name = weatherData.city.name;
        return (
            <tr key={name}>
                <td>{name}</td>
                <td>{name}</td>
                <td>{name}</td>
            </tr>
        )    
    }

    render() {
        return (
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th>City</th>
                        <th>Temperature</th>
                        <th>Pressure</th>
                        <th>Humidity</th>
                    </tr>
                </thead>
                <tbody>
                {this.props.weather.map(this.renderCity)}
                </tbody>
            </table>
        );
    }
}

const mapStateToProps = ({weather}) => {
    return { weather }
};

export default connect(mapStateToProps)(WeatherList);
