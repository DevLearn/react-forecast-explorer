
export type City = {
    id: number,
    name: string,
    country: string,
    coord: any
};

export type WeatherData = {
    city: City,
    cnt: number,
    cod: string,
    list: Array
}

export type WeatherResponse = {
    data: WeatherData
}