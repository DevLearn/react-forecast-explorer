/* @flow */

import {WeatherResponse} from "./types_weather_api";

export type FetchWeatherAction = {
    type: string,
    payload: WeatherResponse
}
