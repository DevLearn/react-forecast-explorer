/* @flow */

import {FETCH_WEATHER, FetchWeatherAction} from "../actions/index";

export default (state = [], action: FetchWeatherAction) => {

    switch (action.type) {
        case FETCH_WEATHER:
            return [action.payload.data, ...state];
        default:
            break;
    }

    return state;
}